module.exports = {
    singleQuote: true,
    jsxBracketSameLine: true,
    arrowParens: 'avoid',
};

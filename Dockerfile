FROM alpine

RUN apk update && apk add --no-cache nodejs npm
RUN npm i -g pnpm

WORKDIR /src
COPY . .

RUN pnpm install
RUN cd /src/client && pnpm install
RUN cd /src/server && pnpm install

RUN cd /src/client && pnpm build

CMD cd /src/server && pnpm start

import {
    Person,
    CrossroadsMap,
    PathfindRequest,
    PathfindResponse,
    Point,
} from '../../common/src';

class CanvasDrawer {
    private context: CanvasRenderingContext2D;

    constructor(map: CrossroadsMap) {
        let canvas = document.getElementById('map') as HTMLCanvasElement;
        let context = canvas.getContext('2d')!;
        this.context = context as CanvasRenderingContext2D;
        this.configStyles();

        this.drawMap(map);
    }

    configStyles(): void {
        this.context.lineCap = 'round';
        this.context.lineJoin = 'round';
        this.context.strokeStyle = 'black';
        this.context.lineWidth = 1;
    }

    drawLinedRoad(from: Point, to: Point): void {
        this.context.beginPath();
        this.context.moveTo(from.x, from.y);
        this.context.lineTo(to.x, to.y);
        this.context.stroke();
    }

    drawCircledCrossroad(x: number, y: number, size: number = 5): void {
        this.context.beginPath();
        this.context.arc(x, y, size, 0, 2 * Math.PI);
        this.context.fill();
    }

    drawCircledPerson(person: Person): void {
        this.context.fillStyle = 'blue';

        this.context.beginPath();
        this.context.arc(
            person.position.x,
            person.position.y,
            5,
            0,
            2 * Math.PI
        );
        this.context.fill();

        this.context.fillStyle = 'black';
    }

    drawMap(graph: CrossroadsMap): void {
        for (const road of graph.roads) {
            const from = graph.crossroads[road.from];
            const to = graph.crossroads[road.to];

            this.drawLinedRoad(from.position, to.position);

            for (const person of road.people) {
                this.drawCircledPerson(person);
            }
        }

        for (const crossroads of graph.crossroads) {
            this.drawCircledCrossroad(
                crossroads.position.x,
                crossroads.position.y
            );
        }
    }

    drawDeWay(lines: Array<Point>): void {
        this.context.strokeStyle = 'red';
        this.context.lineWidth = 5;

        for (let i = 1; i < lines.length; i++) {
            this.drawLinedRoad(lines[i - 1], lines[i]);
        }

        this.context.strokeStyle = 'black';
        this.context.lineWidth = 1;
    }
}

async function pathfind(request: PathfindRequest): Promise<PathfindResponse> {
    const response = await fetch('/pathfind', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(request),
    });

    const json = await response.json();

    return json as PathfindResponse;
}

class DataParser {
    private unparsedElement: HTMLInputElement;

    constructor() {
        this.unparsedElement = document.getElementById(
            'json-map'
        ) as HTMLInputElement;
        this.unparsedElement.addEventListener(
            'change',
            this.process.bind(this)
        );
    }

    process() {
        const reader = new FileReader();
        reader.onload = _ => this.route(JSON.parse(<string>reader.result));
        reader.readAsText(this.unparsedElement.files![0]);
    }

    async route(needed: PathfindRequest) {
        new CanvasDrawer(needed.map).drawDeWay((await pathfind(needed)) || []);
    }
}

new DataParser();

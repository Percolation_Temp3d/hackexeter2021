export type Point = {
    x: number;
    y: number;
};

export type Person = {
    position: Point;
    hasMask: boolean;
    hasCorona: boolean;
};

export type Road = {
    people: Array<Person>;
    from: number;
    to: number;
};

export type Crossroad = {
    position: Point;
};

export type CrossroadsMap = {
    crossroads: Array<Crossroad>;
    roads: Array<Road>;
};

export type PathfindRequest = {
    from: Crossroad;
    to: Crossroad;
    safetyCoeff: number;
    map: CrossroadsMap;
};

export type PathfindResponse = Array<Point> | null;

export const PointSchema = {
    type: 'object',
    properties: { x: { type: 'number' }, y: { type: 'number' } },
    required: ['x', 'y'],
} as const;

export const PersonSchema = {
    type: 'object',
    properties: {
        position: PointSchema,
        hasMask: { type: 'boolean' },
        hasCorona: { type: 'boolean' },
    },
    required: ['position', 'hasMask', 'hasCorona'],
} as const;

export const RoadSchema = {
    type: 'object',
    properties: {
        people: { type: 'array', items: PersonSchema },
        from: { type: 'number' },
        to: { type: 'number' },
    },
    required: ['people'],
} as const;

export const CrossroadsSchema = {
    type: 'object',
    properties: {
        position: PointSchema,
    },
} as const;

export const MapSchema = {
    type: 'object',
    people: {
        type: 'object',
        properties: {
            crossroads: {
                type: 'array',
                items: CrossroadsSchema,
            },
            roads: {
                type: 'array',
                items: RoadSchema,
            },
        },
    },
} as const;

export const PathfindRequestSchema = {
    type: 'object',
    properties: {
        from: { type: 'number' },
        to: { type: 'number' },
        safetyCoeff: { type: 'number' },
        map: MapSchema,
    },
    required: ['from', 'to', 'safetyCoeff', 'map'],
} as const;

export const PathfindResponseSchema = {
    type: 'array',
    items: PointSchema,
} as const;

export const PathfindSchema = {
    body: PathfindRequestSchema,
    response: {
        200: PathfindResponseSchema,
        500: { type: 'null' },
    },
} as const;

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.simpleFlake {
      inherit self nixpkgs;
      name = "hackexeter2021";
      shell = { pkgs }:
        pkgs.mkShell {
          buildInputs = with pkgs; [
            git
            heroku
            nodejs
            nodePackages.pnpm
            nodePackages.typescript
            nodePackages.prettier
          ];
        };
    };
}

import fastify from 'fastify';
import fastify_static from 'fastify-static';
import * as path from 'path';
import { FromSchema } from 'json-schema-to-ts';
import {
    CrossroadsMap,
    PathfindRequestSchema,
    PathfindSchema,
} from '../../common/src';
import { pathfind } from './pathfinder';

const server = fastify();
const port = process.env.PORT || 8080;

server.register(fastify_static, {
    root: path.join(__dirname, '../../client/dist'),
});

server.post<{ Body: FromSchema<typeof PathfindRequestSchema> }>(
    '/pathfind',
    {
        schema: PathfindSchema,
    },
    async (req, res) => {
        const from = req.body.from;
        const to = req.body.to;
        const safetyCoeff = req.body.safetyCoeff;
        const map = <CrossroadsMap>req.body.map;

        const path = pathfind(from, to, safetyCoeff, map);

        if (path == null) {
            res.code(500);
            res.send(null);
        }

        res.send(path);
    }
);

server.listen(port, '0.0.0.0', (err, address) => {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    console.log(`Server is listening at ${address}`);
});

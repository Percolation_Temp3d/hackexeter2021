import { CrossroadsMap, Person, Point } from '../../common/src';
import PriorityQueue from 'ts-priority-queue';

type RoadWeight = { length: number; danger: number };

function distance(from: Point, to: Point): number {
    return Math.sqrt((from.x - to.x) ** 2 + (from.y - to.y) ** 2);
}

function personDanger(person: Person): number {
    if (person.hasCorona) {
        return person.hasMask ? 2 : 8;
    } else {
        return person.hasMask ? 0 : 1;
    }
}

function constructRoadWeights(map: CrossroadsMap): Array<RoadWeight> {
    let weights: Array<RoadWeight> = map.roads.map((road) => ({
        length: distance(
            map.crossroads[road.from].position,
            map.crossroads[road.to].position
        ),
        danger: road.people.reduce(
            (acc, person) => acc + personDanger(person),
            0
        ),
    }));

    return weights;
}

export function pathfind(
    from: number,
    to: number,
    safetyCoeff: number,
    map: CrossroadsMap
): Array<Point> | null {
    const weights = constructRoadWeights(map);

    const fromPoint = map.crossroads[from].position;
    const toPoint = map.crossroads[to].position;

    const crumbs = new Map<number, number>();
    const dist = new Map<number, number>();
    const heuristic = new Map<number, number>();
    const open = new PriorityQueue<number>({
        comparator: (a, b) => heuristic.get(b)! - heuristic.get(a)!,
    });

    dist.set(from, 0);
    heuristic.set(from, distance(fromPoint, toPoint));
    open.queue(from);

    console.log(`Traversing from ${from} to ${to}`);

    while (open.length > 0) {
        const curr = open.dequeue();

        console.log(`curr = ${curr}`);

        if (curr == to) {
            console.log(`Got path`);

            let currCrossroad = curr;
            let path: Array<Point> = [];
            while (currCrossroad != from) {
                path.push(map.crossroads[currCrossroad].position);
                currCrossroad = crumbs.get(currCrossroad)!;
            }
            path.push(map.crossroads[from].position);
            path.reverse();

            console.dir(path);
            return path;
        }

        for (const neigh of Array.from(map.roads.keys()).filter(
            (road) => map.roads[road].from == curr || map.roads[road].to == curr
        )) {
            const neighId =
                map.roads[neigh].from == curr
                    ? map.roads[neigh].to
                    : map.roads[neigh].from;

            const neighDist =
                weights[neigh].length * (1 - safetyCoeff) +
                weights[neigh].danger * safetyCoeff;

            console.log(`Neighbour: ${curr} => ${neighId}`);

            let newDist = dist.get(curr)! + neighDist;

            console.log(`Distance ${dist.get(neighId)} => ${newDist}`);

            if (!dist.has(neighId) || newDist < dist.get(neighId)!) {
                heuristic.set(
                    neighId,
                    newDist +
                        distance(map.crossroads[neighId].position, toPoint)
                );
                crumbs.set(neighId, curr);
                dist.set(neighId, newDist);
                open.queue(neighId);

                console.log(`Queued ${neighId}`);
            }
        }
    }

    return null;
}
